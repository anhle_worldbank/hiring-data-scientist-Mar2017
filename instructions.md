At the Competitive Cities team, we want to see how the World Bank's portfolio of city projects has evolved over time.

To answer this question, you will use two data files

- `WorldBank_list_of_projects.csv`: Clone from this Gitlab repo, gitlab.com/anhle_worldbank/hiring-data-scientist-Mar2017. The column names should be self-explanatory. `GeoLocID` refers to the location of the project, using `geonames.org`'s ID system.
- `geonames.org` gazetteer: Download file `allCountries.zip` from download.geonames.org/export/dump/. Unzipping gives you a table, whose columns are explained on the same page. (Search for `The main 'geoname' table has the following fields :`).
- Feel free to merge in any other public dataset

We classify a project as a "city project" if **one or many** of its `GeoLocID` has the feature class "P", i.e. city, village, ... If there is no `GeoLocID`, we classify that project as not a "city project".

**Deliverable**: A few graphs (more than 1, no more than 4) that show the changes of the city portfolio at the World Bank over time. It's up to you from which angle to examine this change. Show all the data management steps leading to the graphs. There should be a few sentences narrating the graphs. 

To deliver your work:

- Send us both your R script and the output (graphs + text). The output can be in any common file format (.docx, .pdf, .html). Feel free to use `knitr` and the like.
- Invite `anhle_worldbank` to the *private* `gitlab.com` repository where you store your work. (You want it to be private so that others cannot see your work.)

We are looking for: 1) communicated insight, 2) legible graphs, 3) clean code (for example, adv-r.had.co.nz/Style.html), and 4) reproducible analysis (anyone should be able to take your code and run from start to finish and get the same result).

We expect this task to take no more than 5 hours to complete. 

